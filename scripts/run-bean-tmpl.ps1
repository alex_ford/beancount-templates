# Windows PowerShell script for lauching Beancount Templates.

# Author: Alex Richard Ford (arf4188@gmail.com)
# Website: http://www.alexrichardford.com
# License: MIT License

$SCRIPTS_DIR = $PSScriptRoot
$PROJECT_DIR = Split-Path -Parent $SCRIPTS_DIR

$CURRENT_DIR = Get-Location

Set-Location $PROJECT_DIR

pipenv run main $args

Set-Location $CURRENT_DIR
