#!/bin/bash

# Author: Alex Richard Ford (arf4188@gmail.com)

THIS_SCRIPT="$(realpath $0)"
SCRIPTS_DIR="$(dirname ${THIS_SCRIPT})"
PROJECT_DIR="$(dirname ${SCRIPTS_DIR})"

CWD="$(pwd)"
cd "${PROJECT_DIR}" &&
    pipenv run main $@
cd "${CWD}"
