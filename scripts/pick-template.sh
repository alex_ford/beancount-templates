#!/bin/bash

# A simple demonstration of using the unix tool `pick` to select
# the desired template file to use.

ls --ignore=TEMPLATES.md \
    --ignore=_* \
    ~/Data/Personal/beancount-data/templates/ | pick
