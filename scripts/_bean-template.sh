#!/bin/bash
# source me!

# Example in user's ~/.bashrc file:
#    source /path/to/this/script/_bean-template.sh
#
# Author: Alex Richard Ford (arf4188@gmail.com)

export USER_TEMPLATES_DIR="/home/alex/Workspaces-Personal/beancount/beancount-data/templates/"
export USER_DATA_DIR="/home/alex/Workspaces-Personal/beancount/beancount-data/"
export BEAN_TEMPLATES_HOME="/home/alex/Workspaces-Personal/beancount/beancount-templates/"

function bean-template() {
    ORIGIN_DIR="$(pwd)"
    cd ${BEAN_TEMPLATES_HOME} &&
        echo "hello, foobar!" &&
        cd ${ORIGIN_DIR}
}

alias bean-tmpl="bean-template"
