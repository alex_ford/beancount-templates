"""Tests (pytest) for the CurrentMonthEOFFiler."""

import pytest

from beancount_templates.current_month_eof_filer import CurrentMonthEOFFiler

import arrow

def test_determine_data_file_path():
    # we **must** use the .to('local') version here, as Beancount is TZ agnostic/unaware
    # and we are assuming that the user has recorded everything in their primary timezone
    # by default, Arrow will spit out UTC times, and so this can screw up different date/time
    # operations if not converted to a consistent timezone.
    cur_date: arrow.Arrow = arrow.now().to('local')
    print(f"Current date: {cur_date}")
    # and here is what we should get for the _out_file
    expected_out_file: str = f"../beancount-data/transactions/{cur_date.year}/{cur_date.year}{cur_date.month:02}.beancount"
    filer = CurrentMonthEOFFiler()
    print(f"Out file: {filer._out_file}")
    assert filer._out_file == expected_out_file

def test_write():
    cur_date: arrow.Arrow = arrow.get()
    print(f"Current date: {cur_date}")
    filer = CurrentMonthEOFFiler()
    data = \
        "2019-01-01 ! \"TEST\"   \"TEST TEST TEST\"\n" + \
        "  Assets:AccountA                0.00 USD\n" + \
        "  Assets:AccountB                0.00 USD\n"
    filer.write(data)
