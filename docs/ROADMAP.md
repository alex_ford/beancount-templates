# ROADMAP

The following list captures future improvements and capabilities for the Beancount Templates project:

* Insertion into Beancount data file (R1)
    * The transaction is inserted at the end of the data file.


* Insertion into Beancount data file (R2)
    * The transaction is inserted at the correct spot in the data file.


* Look-up selection for existing account names when using `bean-tmpl`


* Beancount Data file processing
    * Provide a Beancount Data file that contains templated expressions within it and Beancount Template will process it!
    * The results will be saved back to the same file. (In-place saving/processing!)


* Date Math
    * `{{ DATE + 1 }}`
        * Adds the specified number of days to the date.
    * `{{ YEAR-(MONTH+1)-DAY }}`
        * Adds the specified number to the specified date field.


* Transaction Validation
    * Early validation before inserting into Beancount data file.


* Default Values
    * Support for Jinja2's default value functionality.
