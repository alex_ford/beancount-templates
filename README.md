# Beancount Templates (beancount-templates)

This project provides a templating mechanism for use with the Beancount text-based accounting system.

Beancount Templates is part of a larger collection of Beancount support projects. // TODO: insert link to Gitlab project group

**Author**: Alex Richard Ford (arf4188@gmail.com)

**Website**: <http://www.alexrichardford.com>

**License**: MIT License (see [LICENSE](LICENSE) file for more details)

## What are Beancount Templates?

This project provides a templating mechanism for use with the Beancount text-based accounting system. A Beancount Template (or just "Template") is one or more Beancount Directives (usually Transaction Directives) that should be entered/processed together. In the most simple case, a Template can be a single transaction and will be marked up with special variable expressions that are replaced by the Template engine. More advanced uses are still quite simple: maybe you have two or three transactions that must be entered together. No problem, just provide them in your template file and you're good to go! The Template Engine can also generate matching tags and links if you set them up.

When the user executes the Template Engine, the "auto-replaced fields" will be automatically replaced with the corresponding value for that field. Other fields will get their value from prompts to the user on the command line. Last, but not least, "evaluated" fields will be evaluated (mathematically and/or programmatically) and inserted in place of the field.

The Template Engine relies on the [Jinja2](http://jinja.pocoo.org/docs/2.10/) template framework for Python.

## System Requirements/Dependencies

In addition to this Git repo, you will also need the following other repos/libraries:

- Python3
- Pip
- Pipenv

### Managed Dependencies

These are also required, and managed by `pipenv` so just simply ensure your Pipenv virtual environment is setup correctly and these will be satisfied:

- Beancount
- Jinja2

## Quick Start Options

### Quick Start using Prompted Scripts

Simply execute `python .\add-gdax.py` to receive a prompted set of questions to help enter the templated transaction! This acts more like a wizard, or guided entry system.

Execute the more powerful and flexible `template-proc.py` script with a template file that contains the beancount template to use: `py .\template-proc.py templates\generic-tx.bctl` This will prompt you for each field.

### Quick Start using `bean-tmpl`

`bean-tmpl` is an executable command line tool that generically uses Jinja2 to process plain text files, specifically Beancount plain-text directive files. Install it via `pipenv install` and use it like so:

```bash
bean-tmpl --help
```

```bash
bean-tmpl
Choose from the following registered templates:
1: template1
2: template2
3: template3
4: template4
```

This will simply process the Jinja2 within the file and dump the result to the console.

## Common Conventions

In all of my beancount-\* repos, I follow the following conventions:

-   Files recognized by my beancount microcosm:

| Extension    | Description                                                                                                            |
| ------------ | ---------------------------------------------------------------------------------------------------------------------- |
| `.beancount` | Beancount Data file. This is where your actual beancount directives live. Template files reuse this extension as well. |
| `.spec`      |                                                                                                                        |
| `.conf`      | Configuration file.                                                                                                    |

## Field Reference

| Field Type             | Description                                                                                                                                                                                                                                                              | Example      |
| ---------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ | ------------ |
| Basic Field            | The template processor will prompt the user for the value of the named field. The prompt will only happen once for each uniquely named field, so if you use the same field name multiple times, you can essentially insert the same value multiple times as you see fit! | `${field}`   |
| Auto-replacement Field | Certain keywords are recognized by the template processor and in this case, instead of prompting the user for the value, the processor will use certain logic to come up with the value.<br />See Auto-Replaced Fields Reference                                         | `${keyword}` |
| Evaluation Field       | With these fields, the template processor will evaluate the math expression given in the field. This supports basic arithmetic, and order of operation controls.                                                                                                         | `${(1+1)}`   |

## Auto-Replacement Field Reference

| Field                                                     | Description                                                                         |
| --------------------------------------------------------- | ----------------------------------------------------------------------------------- |
| `{{ TODAY }}`                                             | Inserts the current date in the form `YYYY-MM-DD`.                                  |
| `{{ DATE }}`                                              | Synonym of `{{ DATE }}`.                                                            |
| `{{ TIME }}`                                              | Inserts the current time in the form `HH:MM TZ` in 24 hour format.                  |
| `{{ TIME:UTC }}`                                          | Inserts the current time like the above, but for the specified time zone (e.g. UTC) |
| `{{ DATETIME }}`                                          | Inserts the current date and time, like the above.                                  |
| `{{ FLAG }}`                                              | One of the choices: "!" or "\*"                                                     |
| `{{ LINK }}`, `{{ LINK1 }}`, `{{ LINK2 }}`, `{{ LINK3 }}` |                                                                                     |

## Possible Fields Implemented in the Future

| Field              | Description                                                      |
| ------------------ | ---------------------------------------------------------------- |
| `{{ posting[N] }}` | Makes a reference to the posting `N` of the current transaction. |
|                    |                                                                  |

## Simple Math

Jinja2 supports math expressions within `{{ }}` statements, and so, declaring one will work as expected.

```beancount
2019-01-01 ! "Payee"   "Narration"
  Assets:Account1           -{{ 1+2+3 }} USD
  Expenses:Shopping
```

## Decimal Precision/Formatting

... coming soon

## Validated and Completion Assist Fields

These fields require user input, but aren't freeform. They either must pass a validation test to ensure they don't contain any illegal characters, or they must be one of a predefined list of values.

| Field                        | Description                                                                                                                                                                                                                 |
| ---------------------------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `{{ posting[N] }}`           | This is a meta field which holds the set of fields for the numbered posting. It is zero index based, so the first posting is 0.                                                                                             |
| `{{ posting[N].account }}`   | The template engine will provide the predefined accounts from the user's beancount data file, if it was specified either in the config or by the command line option.                                                       |
| `{{ posting[N].value }}`     | The template engine will validate that this is a REAL NUMBER.                                                                                                                                                               |
| `{{ posting[N].commodity }}` | The template engine will validate that this is a commodity that has been previously defined in the user's data file and is acceptable by the account, if the account was set to limit the types of commodities it can hold. |

One common usage of the above is the ability to invert the value and/or perform other calculations in order to compute the value of the subsequent postings! For example:

```beancount
    2018-06-17 ! "Payee" "Narration"
      Asset:Account1    100.00 USD
      Income:Paycheck   -50.00 USD
      Income:Tips       -50.00 USD
```

The above can be specified as a template:

```beancount
    {{ today }} {{ flag }} "{{ payee }}" "{{ narration }}"
      Asset:Account1    ${posting[0].value} USD
      Income:Paycheck   ${(-1 * posting[0].value / 2)} USD
      Income:Tips       ${(-1 * posting[0].value / 2)} USD
```

In this template, we've instructed the engine to automatically fill in today's date. Then a lot of literal (or static) stuff, until we get to the values. The engine will only prompt one time, for the value of `posting[0].value` and then you're done! Assuming you gave "100.00" as the value, the engine would compute the other fields and you would get the beancount directive above.
