# This script helps by guiding the data entry for trading on GDAX.
# It also serves to codify the conventions I've employed for tracking
# GDAX, and generally, crypto trades.
#
# Notes on conventions:
#  - The cost will always be in USD and will always be given with 2 sigfigs
#  - When the above results in a remainder that causes the transaction to not
#       balance, a Trade Conversion entry is added. (It should always be for
#       a very, very small amount of USD. Less than a penny! Often less than 1/100th.)
#  - A fee, if provided, will be output as an additional posting on the USD
#       account with a comment indicating it is the fee
#  - A fee, if provided, will be added to the price paid, when computing the
#       cost basis of the trade, so only 1 posting will reflect the other
#       side of the balance equation
#
# Testing
#  - Tests are stored in the add-gdax-test.dat file and simply represent
#       newline separated inputs to this script.
#  - Use it with the following: cat add-gdax-test.dat | python add-gdax.py
#
# @author Alex Ford (arf4188@gmail.com)

import datetime

beancountDataFile = r"..\beancount-data\ledger.beancount"

# Example of a full beancount entry:
# 2018-04-17 * "GDAX" "Bought Litecoin"
#   Assets:Investments:GDAX:Cash                                           -10.07975997 USD
#   Assets:Investments:GDAX:Cash                                            -0.03023928 USD ; fee
#   Assets:Investments:GDAX:Litecoin                                         0.07625783 LTC { 132.58 USD, 2018-04-17 }
#   Equity:Trade-Conversion                                               -0.0002638514 USD
cont = True
while cont:
    orderType = input("Is this a (b)uy or (s)ell? ")
    print(orderType)
    crypto = input("Which crypto? (BTC, BCH, ETH, LTC) ")
    print(crypto)
    crypto = crypto.upper()
    if crypto == "BTC":
        cryptoName = "Bitcoin"
    elif crypto == "BCH":
        cryptoName = "Bitcoin-Cash"
    elif crypto == "ETH":
        cryptoName = "Ethereum"
    elif crypto == "LTC":
        cryptoName = "Litecoin"
    cryptoAmt = float(input("How much crypto? "))
    print(cryptoAmt)
    usdAmt = float(input("For how much USD? "))
    print(usdAmt)
    feeAmtStr = input("How much was the fee in USD? (Leave blank for no fee.) ")
    print(feeAmtStr)
    if feeAmtStr != "":
        feeAmt = float(feeAmtStr)

    # TODO provide option to enter different datetime instead of 'now'
    dateObj = datetime.datetime.now()
    dateStr = dateObj.strftime("%Y-%m-%d")
    # TODO DEFECT: this is not providing the timezone
    timeStr = dateObj.strftime("%H:%M %Z")#"21:05 EST"
    costBasis = usdAmt / cryptoAmt

    beancountStr = dateStr + " * \"GDAX\" "
    if orderType == "b":
        beancountStr += "\"Bought " + crypto + "\"\n"
        beancountStr += "  time: \"" + timeStr + "\"\n"
        beancountStr += "  Assets:Investments:GDAX:USD    -" + str(usdAmt) + " USD\n"
        beancountStr += "  Assets:Investments:GDAX:" + cryptoName + "    " + str(cryptoAmt) + " " + crypto + " { " + "{:0.2f}".format(costBasis) + " USD, " + dateStr + " }"
        # TODO handle computing the Equity:Trade-Conversion automatically
        # TODO handle the fee computation(s)
    elif orderType == "s":
        print("sell")
        # TODO provide sell transaction support
    else:
        print("Unrecognized order type!")
        exit

    # TODO provide argparser that implements the --save argument feature
    # Save the result to the beancount data file
    # The --save argument feature will assume 'y' to this prompt instead of asking the user.
    print(beancountStr + "\n")
    resp = input("Would you like to add this to your beancount data file? (y/n) ")
    if resp == "y":
        with open(beancountDataFile, "a") as bcdat:
            bcdat.write("\n" + beancountStr + "\n")

    # Provide the option to restart entry for additional entries if the user wants
    resp = input("Do you have another transaction to enter? (y/n) ")
    if resp == "n":
        cont = False

print("Goodbye!")