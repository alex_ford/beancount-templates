"""Filer that simply appends the given text to the end of the current monthly
file stored in the data directory.

Author: Alex Richard Ford (arf4188@gmail.com)
Website: http://www.alexrichardford.com
License: MIT License
"""

import arrow
import os

from typing import List

# TODO: use colorlog and separate logging into appropriate levels

class CurrentMonthEOFFiler():
    """Filer that determines the current month, then determines the path to the file for that month."""

    _out_file: str = None

    def __init__(self):
        """Init method."""
        self._out_file = self._determine_data_file_path()
    
    def _determine_data_file_path(self) -> str:
        cur_date: arrow.Arrow = arrow.get().to('local')
        _DATA_DIR = os.path.expanduser("~/Data/Personal/beancount-data/")
        return f"{_DATA_DIR}/transactions/{cur_date.year}/{cur_date.year}{cur_date.month:02}.beancount"

    def write(self, data: str):
        """Write the specified string 'data' to the end of the current month file."""
        print(f"Writing to: {self._out_file}")
        # splits the string on newline characters so we can do a little formatting
        data_lines = List[str]
        data_lines = data.splitlines()
        with open(self._out_file, "a") as of:
            for dl in data_lines:
                print(f" | {dl}")
                of.write(dl)
                of.write("\n") # splitlines() removes line endings, so we need to put them back
            # add a blank line after the transaction for better aesthetics
            of.write("\n")
