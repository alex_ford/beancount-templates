import uuid
import colorlog

_log = colorlog.getLogger(f"beancount_templates.{__name__}")

class SUUID():
    """Functions for the creation of short UUIDs."""

    def __init__(self):
        pass

    @staticmethod
    def generate():
        """Generate a new random 8 hexidecimal character SUUID."""
        retval = uuid.uuid4().hex
        _log.debug(f"retval = {retval}")
        retval = retval[:8]
        _log.debug(f"retval = {retval}")
        return retval

if __name__ == "__main__":
    """For one-off testing purposes only!"""
    sh = colorlog.StreamHandler()
    _log.addHandler(sh)
    _log.setLevel("DEBUG")
    SUUID.generate()
