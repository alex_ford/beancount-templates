"""Beancount Templates entry point for invoking the Template Processor.

    This script takes in a Beancount Template file and provides an interactive
    series of console prompts in order to fill out the template fields, as well as
    processing other fields programmatically.

    Author: Alex Richard Ford (arf4188@gmail.com)
    Website: http://www.alexrichardford.com
    License: MIT License (see LICENSE)
"""

import re
import os

import colorlog
import arrow
from jinja2 import Template, Environment, meta

from suuid import SUUID

from current_month_eof_filer import CurrentMonthEOFFiler

_log = colorlog.getLogger(f"beancount_templates.{__name__}")

# TODO: move this to some external configuration
_DATA_DIR = os.path.expanduser("~/Data/Personal/beancount-data/")
_TMPL_DIR = f"{_DATA_DIR}/templates/"

# Generally speaking, all template fields are enclosed in '{{ }}'. There
# are different types of replacement fields as follows:
#
# [[ Feature status: COMPLETE ]]
# Basic Replacement Fields
# ${} are basic replacement fields. You can specify multiple of the same
# replacement field in the same template in order to have a value
# repeated where it is needed. At runtime, the field will only be
# prompted to the user once.
#   ${fieldName}    Declares a field 'fieldName' to be replaced
#
# [[ Feature status: COMPLETE ]]
# Auto Replacement Fields
# These are special replacement fields which use a keyword which tells
# the processor to automatically set a value for the field. They are
# displayed the user during prompting so that the values can be confirmed
# or changed. Currently, the following fields exist:
#   ${date}     Set to the current date in YYYY-MM-DD format
#   ${time}     Set to the current time in HHMM format
#
# [[ Feature status: TODO ]]
# Format Validation
# Fields are typically required to match some format in order to be
# valid. Format Validation allows this format to be specified in the
# field so that the processor can check it and reject bad values during
# user input. Validation is specified as follows:
#   ${field|date}               Validates field to be a string in beancount format YYYY-MM-DD (e.g. "2018-07-28")
#   ${field|int}                Validates field to be an integer number. (e.g. "1")
#   ${field|real}               Validates field to be a real number. (e.g. "1.2345")
#
# [[ Feature status: TODO ]]
# Applied Formatting
# Similar to Format Validation, the template can also indicate when a
# value should have a particular formatting applied to it.
#   ${field|real,,"#.##"}         Formats real number so that it includes 2 decimal places
#   ${field|real,,"#.########"}   Formats real number so that it includes 8 decimal places
#
# [[ Feature status: TODO ]]
# ${()} are evaluation fields, which can handle basic calculations
# along with replacements for any non-numeric term. For example,
# you can specify ${(value1/value2)} which the processor will first
# replace value1, then replace value2, then finally evaluating
# value1/value2 according to conventional mathematic rules. You
# may specify additional parenthesis to control order of operations.
# Evaluation always occurs AFTER basic replacement.
#
# [[ Feature status: TODO ]]
# Option Lists
# A preset list of values may be defined in the template which is then
# presented for selection at runtime. In the template, they take the
# form:
#   ${field['value1','value2',...,'valueN']}
#
# [[ Feature status: COMPLETE ]]
# Comments
# You may place comments in the template file by prefixing the line with
# either a pound/hash symbol ("#") or a semi-colon symbol (";"). These
# are simply ignored by the processor.
#
# [[ Feature status: TODO ]]
# Save to Beancount Data File
# Indicate -s or --save to save the results to a Beancount data file.
# Providing no argument uses the default ("..\beancount-data\ledger.beancount")
# Specify the path as the argument to save to a different file.
#   py .\template-proc.py --save templateName.beancount
#
# [[ Feature status: COMPLETE ]]
# Eval Math Expressions
# You can enter simple math, like 1+1, into the AMOUNT (or any variable really)
# and the processor will take care of it! Nothing special needs to happen in
# the template file itself, it's purely a user entry thing.


class TemplateProcessor():
    """Processor for Beancount Template files."""

    tmpl_list: list = []

    def __init__(self):
        logHandler = colorlog.StreamHandler()
        logHandler.setFormatter(colorlog.ColoredFormatter())
        _log.addHandler(logHandler)
        _log.setLevel("INFO")

        if not os.path.exists(_TMPL_DIR):
            raise RuntimeError(f"Could not find templates at: {_TMPL_DIR}")
        self.tmpl_list = os.listdir(_TMPL_DIR)
        self.tmpl_list.sort()


    def prompt_selection(self) -> int:
        """Prompts for the user to select one of their templates.

        Templates are loaded from the user's beancount-data/templates directory.

        Returns the index number corresponding to the template file that should be used.
        """
        # Prompt for template selection
        tmplCounter: int = 0
        for template in self.tmpl_list:
            print(f"{tmplCounter:2}: {template}")
            tmplCounter += 1
        tmplSelectionIndex = int(input("Template selection? "))
        return tmplSelectionIndex


    def process_template(self, tmpl_index: int):
        tmplSelection = self.tmpl_list[tmpl_index]
        # Read in template file
        _log.info("Using: " + tmplSelection)
        with open(os.path.join(_TMPL_DIR, tmplSelection)) as tmpl_file:
            contents = tmpl_file.read()
            loaded_template = Template(contents)
        env = Environment()
        ast = env.parse(contents)

        # Process variables which are filled by the processor
        varmap = {}
        datetime_instance = arrow.now()
        varmap['DATE'] = datetime_instance.format("YYYY-MM-DD")
        varmap['TIME'] = datetime_instance.format("HH:mm:ss TZ")
        varmap['DATETIME'] = datetime_instance.format("YYYY-MM-DD HH:mm:ss TZ")

        # Link handling - this supports LINK, LINK1, LINK2, LINK3
        varmap['LINK'] = SUUID.generate()
        varmap['LINK1'] = SUUID.generate()
        varmap['LINK2'] = SUUID.generate()
        varmap['LINK3'] = SUUID.generate()
        # TODO: find all keys that start with 'LINK', followed by a number, and
        # then process them like the above.

        # Any variables that remain undefined should go to the user prompt
        # TODO: impl input prompt handlers that provide for different types of input, along
        # with helpful features, and validation of input
        # - AMOUNT handler
        #       - assumes USD currency, unless commodity code is given
        #       - formats value to 2 sigfigs in most cases
        #       - prefix value with 0 if a fractional amount is given
        #       - rejects non-numerical input for the value
        #       - evals basic math expressions (e.g. 50/2)
        # - ACCOUNT handler
        #       - provides a selection list of the user's accounts to choose from
        #       - allows for entry of new accounts
        #       - allows for fuzzy searching of accounts (type any part of the account name)
        # - BOOLEAN handler
        #       - allows for TRUE or FALSE, selection from choices
        # - DATE handler
        # - TIME handler
        undeclared_vars = meta.find_undeclared_variables(ast)
        _log.debug(f"undeclared_vars = {undeclared_vars}")
        for v in undeclared_vars:
            if v not in varmap:
                user_input = input(f"{v}? ")
                try:
                    user_input = float(user_input)
                except:
                    # attempt to eval as Python
                    try:
                        user_input = eval(user_input)
                    except:
                        pass
                varmap[v] = user_input
        complete_entry = loaded_template.render(varmap)
        print(f"{complete_entry}")

        # Write out to file
        filer = CurrentMonthEOFFiler()
        filer.write(complete_entry)
        _log.info("Transaction successfully written to file!")
