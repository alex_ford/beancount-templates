#!/usr/bin/env python3

"""Beancount Templates entry point for invoking the Template Processor.

    This script takes in a Beancount Template file and provides an interactive
    series of console prompts in order to fill out the template fields, as well as
    processing other fields programmatically.

    Author:  Alex Richard Ford <arf4188@gmail.com>
    Website: http://www.alexrichardford.com
    License: MIT License (see LICENSE)
"""

import argparse

import colorlog

from template_processor import TemplateProcessor

_log = colorlog.getLogger(f"beancount_templates.{__name__}")


def main():
    """Main function for use by users from the command line."""
    logHandler = colorlog.StreamHandler()
    logHandler.setFormatter(colorlog.ColoredFormatter())
    _log.addHandler(logHandler)
    _log.setLevel("INFO")

    # TODO: replace argparse with Click
    parser = argparse.ArgumentParser(
        description="The Template Processor which " +
        "takes in a template file and processes it in order to create new " +
        "Beancount entries. By default, results are only sent to stdout, so no " +
        "changes are made to your data file(s)."
    )
    with open("./VERSION") as versionFile:
        parser.add_argument(
            "--version",
            action="version",
            version=f"%(prog)s v{versionFile.readline()}"
        )
    parser.add_argument(
        "--debug",
        action="store_true",
        help="Enables DEBUG logging level for more output from the program."
    )
    #parser.add_argument("template")
    # TODO:
    # parser.add_argument("-s", "--save",
    #   help="Save the result to the beancount data file.",
    #   action="store_true")
    # TODO:
    # parser.add_argument("-l", "--list",
    #     help="Lists the available templates along with metadata about them.",
    #     action="store_true")
    # TODO:
    # parser.add_argument("-i", "--include",
    #       nargs="?",
    #       help="Specifies Beancount files to use as import data."

    parser.add_argument("-t", "--template",
            nargs="?",
            help="Use the specified template instead of prompting for selection.")

    args = parser.parse_args()

    # Handle priority args
    if args.debug:
        _log.setLevel("DEBUG")
        _log.debug("DEBUG LOGGING IS ENABLED!")
    _log.debug(f"{args}")

    tproc = TemplateProcessor()
    if args.template != None:
        _log.info(f"OK will use template: {args.template}")
        tmpl_index = int(args.template)
    else:
        tmpl_index = tproc.prompt_selection()
    tproc.process_template(tmpl_index)


if __name__ == "__main__":
    main()
