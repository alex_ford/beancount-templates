#!/usr/bin/env python3

"""Processes Template fielda in a data repository.

This provides a file scanner which can read Beancount data files, looking for Template fields contained within. The fields are processed according to the rules of Beancount Templates and can either be written back to the source file, or to some other location."""
